
package data;

import entites.EspeceCultivee;
import entites.Exploitation;
import entites.Parcelle;
import entites.Produit;
import entites.Pulverisation;
import entites.TraitementEnChamp;
import entites.TraitementSemence;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class Donnees {

   static List<Exploitation>     lesExploitations = new LinkedList();
   static List<Parcelle>         lesParcelles     = new LinkedList(); 
   static List<Produit>          lesProduits      = new LinkedList();  
   static{
   
      Exploitation exp1=new Exploitation("exp001","Dupont Jacques","3 rue du chemin vert");
      Exploitation exp2=new Exploitation("exp002","Durant  Alain ","5 rue  des champs");
      lesExploitations.add(exp1);lesExploitations.add(exp2);
      
      EspeceCultivee espC1= new EspeceCultivee("BLE","Blé","Céréale");
      EspeceCultivee espC2= new EspeceCultivee("COL","Colza","");
      EspeceCultivee espC3= new EspeceCultivee("ORG","Orge","Céréale");
      EspeceCultivee espC4= new EspeceCultivee("PDT","Pomme de terre","");
      EspeceCultivee espC5= new EspeceCultivee("AVO","Avoine","Céréale");   
     
      Parcelle parc1=new Parcelle( "parc01",
                                   utilitaires.UtilDate.chaineVersDate("5/5/2015"),
                                   utilitaires.UtilDate.chaineVersDate("25/07/2015"),
                                   200F,espC1,exp1);
      Parcelle parc2=new Parcelle("parc02",new Date(),new Date(),100F,espC2,exp2);
      Parcelle parc3=new Parcelle("parc03", 
                                  utilitaires.UtilDate.chaineVersDate("10/5/2015"),
                                  utilitaires.UtilDate.chaineVersDate("1/8/2015"),
                                  150F,espC3,exp1);
      Parcelle parc4=new Parcelle("parc04",new Date(),new Date(),250F,espC4,exp2);
      Parcelle parc5=new Parcelle("parc05",
                                  utilitaires.UtilDate.chaineVersDate("14/5/2015"),
                                  utilitaires.UtilDate.chaineVersDate("15/7/2015"),
                                  100F,espC1,exp1);
      
      lesParcelles.add(parc1);lesParcelles.add(parc2);lesParcelles.add(parc3);
      lesParcelles.add(parc4);lesParcelles.add(parc5);
      
      exp1.getLesParcelles().add(parc1);exp1.getLesParcelles().add(parc3);
      exp1.getLesParcelles().add(parc5);
      exp2.getLesParcelles().add(parc2); exp2.getLesParcelles().add(parc4);
       
      Produit p1= new Produit("prod001","AAAAAA");
      Produit p2= new Produit("prod002","BBBBBB");
      Produit p3= new Produit("prod003","CCCCCC");
      Produit p5= new Produit("prod004","DDDDDD");
      Produit p4= new Produit("prod005","DDDDDD");
      Produit p6= new Produit("prod006","FFFFFF");
      Produit p7= new Produit("prod007","GGGGGG");
      
      lesProduits.add(p1);lesProduits.add(p2);lesProduits.add(p3);lesProduits.add(p4);
      lesProduits.add(p5);lesProduits.add(p6);lesProduits.add(p7);
      
      TraitementSemence  ts1= new TraitementSemence("TS01", p6, parc2, 5F, new Date());
      TraitementSemence  ts2= new TraitementSemence("TS02", p7, parc1, 1F, utilitaires.UtilDate.chaineVersDate("25/4/2015"));
      TraitementSemence  ts3= new TraitementSemence("TS03", p7, parc4,  2F, new Date());
      TraitementSemence  ts4= new TraitementSemence("TS04", p6, parc1, 2F, utilitaires.UtilDate.chaineVersDate("30/4/2015"));
      
      parc1.getLesTraitements().add(ts2);
      parc1.getLesTraitements().add(ts4);
      
      parc4.getLesTraitements().add(ts3);
      parc2.getLesTraitements().add(ts1);
      
      
      TraitementEnChamp tec1= new TraitementEnChamp("TC01", p1, parc5);
      TraitementEnChamp tec2= new TraitementEnChamp("TC02", p2, parc1);
      TraitementEnChamp tec3= new TraitementEnChamp("TC03", p3, parc2);
      TraitementEnChamp tec4= new TraitementEnChamp("TC04", p5, parc2);
      TraitementEnChamp tec5= new TraitementEnChamp("TC05", p4, parc1);
      
      parc1.getLesTraitements().add(tec5);
      parc2.getLesTraitements().add(tec4);
      parc3.getLesTraitements().add(tec3);
      parc1.getLesTraitements().add(tec2);
      parc5.getLesTraitements().add(tec1);
      
      p1.getLesTraitements().add(tec1);
      p2.getLesTraitements().add(tec2);
      p3.getLesTraitements().add(tec3);
      p4.getLesTraitements().add(tec5);
      p5.getLesTraitements().add(tec4);
      p6.getLesTraitements().add(ts1);
      p6.getLesTraitements().add(ts4);
      p7.getLesTraitements().add(ts2);
      p7.getLesTraitements().add(ts3);
      
      
      
      Pulverisation pulv1  = new Pulverisation( 1L,new Date(),2F);
      Pulverisation pulv2  = new Pulverisation( 2L,utilitaires.UtilDate.chaineVersDate("5/5/2015"),1F);
      Pulverisation pulv3  = new Pulverisation( 3L,new Date(),2F);
      Pulverisation pulv4  = new Pulverisation( 4L,utilitaires.UtilDate.chaineVersDate("10/5/2015"),1F);
      Pulverisation pulv5  = new Pulverisation( 5L,new Date(),2F);
      Pulverisation pulv6  = new Pulverisation( 6L,utilitaires.UtilDate.chaineVersDate("20/5/2015"),2F);
      Pulverisation pulv7  = new Pulverisation( 7L,new Date(),1.5F);
      Pulverisation pulv8  = new Pulverisation( 8L,utilitaires.UtilDate.chaineVersDate("25/6/2015"),2.5F);
      Pulverisation pulv9  = new Pulverisation( 9L,new Date(),2.5F);
      
      Pulverisation pulv10 = new Pulverisation(10L,new Date(),2F);
      Pulverisation pulv11 = new Pulverisation(11L,new Date(),1.5F);
     
      Pulverisation pulv14 = new Pulverisation(14L,new Date(),2.5F);
      Pulverisation pulv15 = new Pulverisation(15L,new Date(),1F);
      Pulverisation pulv16 = new Pulverisation(16L,new Date(),2F);
      Pulverisation pulv17 = new Pulverisation(17L,utilitaires.UtilDate.chaineVersDate("12/5/2015"),2F);
      Pulverisation pulv18 = new Pulverisation(18L,utilitaires.UtilDate.chaineVersDate("18/5/2015"),1.5F);
      Pulverisation pulv19 = new Pulverisation(19L,utilitaires.UtilDate.chaineVersDate("17/6/2015"),2F);
      Pulverisation pulv20 = new Pulverisation(20L,utilitaires.UtilDate.chaineVersDate("20/6/2015"),3F);
     
     
      tec1.getLesPulverisations().add(pulv1);
      tec1.getLesPulverisations().add(pulv3);
      tec1.getLesPulverisations().add(pulv5);
      tec1.getLesPulverisations().add(pulv7);
      
      tec2.getLesPulverisations().add(pulv2);
      tec2.getLesPulverisations().add(pulv4);
      tec2.getLesPulverisations().add(pulv6);
      tec2.getLesPulverisations().add(pulv8);
      
      tec3.getLesPulverisations().add(pulv9);
      tec3.getLesPulverisations().add(pulv11);
      tec3.getLesPulverisations().add(pulv15);
 
      tec4.getLesPulverisations().add(pulv10);
      tec4.getLesPulverisations().add(pulv14);
      tec4.getLesPulverisations().add(pulv16);
      
      tec5.getLesPulverisations().add(pulv17);
      tec5.getLesPulverisations().add(pulv18);
      tec5.getLesPulverisations().add(pulv19);
      tec5.getLesPulverisations().add(pulv20);
   }  
}
