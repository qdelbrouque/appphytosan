
package entites;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;

@Entity

public class EspeceCultivee implements Serializable {
    
    @Id
    private String         idEsp;
    private String         nomEsp;
    private String         typeEsp;

    public EspeceCultivee() { }

    public EspeceCultivee(String idEsp, String nomEsp, String typeEsp) {
        
        this.idEsp = idEsp;
        this.nomEsp = nomEsp;
        this.typeEsp = typeEsp;
    }
   
    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public String getIdEsp() {
        return idEsp;
    }
    
    public void setIdEsp(String idEsp) {
        this.idEsp = idEsp;
    }
    
    public String getNomEsp() {
        return nomEsp;
    }
    
    public void setNomEsp(String nomEsp) {
        this.nomEsp = nomEsp;
    }
    
    public String getTypeEsp() {
        return typeEsp;
    }
    
    public void setTypeEsp(String typeEsp) {
        this.typeEsp = typeEsp;
    }
   
    //</editor-fold>
}



