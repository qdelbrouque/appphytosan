package entites;
import java.io.Serializable;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;

@Entity

public class Parcelle implements Serializable {

    @Id
    private String           idParc;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date             dateSemis;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date             dateRecoltePrevue;
    private Float            surface; // exprimée en hectares
    
    @JoinColumn( name = "CODEESPECECULTIVEE")
    @ManyToOne
    private EspeceCultivee   lEspeceCultivee;
    @JoinColumn( name = "CODEEXPLOITATION")
    @ManyToOne
    private Exploitation     lExploitation;
    @OneToMany(mappedBy = "laParcelle")
    private List<Traitement> lesTraitements;

    // poidsProduitUtilise
    // calcule et retourne le poids total du produit dont l'identifiant pIdProd est passé en paramètre
    // utilisé dans les traitements ( sur semence ou en champ) concernant la parcelle
    
    public Float             poidsProduitUtilise(String pIdProd){
    
     Float total=0F;
     
     for ( Traitement t : lesTraitements){
     
         if( t.getLeProduit().getIdProd().equals(pIdProd) ){
           total+=t.quantiteAppliquee();
         }
     }
     
     return total;
    
    }
     

    public Parcelle() {
    
       this.lesTraitements= new LinkedList<Traitement>();
    }

    public Parcelle(String idParc, Date dateSemis, Date dateRecoltePrevue, Float surface, EspeceCultivee lEspeceCultivee, Exploitation lExploitation) {
        
        this();
        this.idParc            = idParc;
        this.dateSemis         = dateSemis;
        this.dateRecoltePrevue = dateRecoltePrevue;
        this.surface           = surface;
        this.lEspeceCultivee   = lEspeceCultivee;
        this.lExploitation     = lExploitation;
    }
     
    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public EspeceCultivee getlEspeceCultivee() {
        return lEspeceCultivee;
    }
    
    public void setlEspeceCultivee(EspeceCultivee lEspeceCultivee) {
        this.lEspeceCultivee = lEspeceCultivee;
    }
    
    public Exploitation getlExploitation() {
        return lExploitation;
    }
    
    public void setlExploitation(Exploitation lExploitation) {
        this.lExploitation = lExploitation;
    }
    
    public List<Traitement> getLesTraitements() {
        return lesTraitements;
    }
    
    public void setLesTraitements(List<Traitement> lesTraitements) {
        this.setLesTraitements(lesTraitements);
    }
    
     public String getIdParc() {
        return idParc;
    }

    public void setIdParc(String idParc) {
        this.idParc = idParc;
    }

    public Date getDateSemis() {
        return dateSemis;
    }

    public void setDateSemis(Date dateSemis) {
        this.dateSemis = dateSemis;
    }

    public Date getDateRecoltePrevue() {
        return dateRecoltePrevue;
    }

    public void setDateRecoltePrevue(Date dateRecoltePrevue) {
        this.dateRecoltePrevue = dateRecoltePrevue;
    }

    public Float getSurface() {
        return surface;
    }

    public void setSurface(Float surface) {
        this.surface = surface;
    }

    //</editor-fold>   
}