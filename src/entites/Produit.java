package entites;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity

public class Produit implements Serializable {
    
    @Id
    private String           idProd;
    private String           nomprod;
    
    @OneToMany(mappedBy = "leProduit")
    private List<Traitement> lesTraitements;

    public Produit() {
    
      lesTraitements= new LinkedList<Traitement>();
    }

    public Produit(String idProd, String nomprod) {
        
      this();
      this.idProd = idProd;
      this.nomprod = nomprod;
    }

    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public List<Traitement> getLesTraitements() {
        return lesTraitements;
    }
    
    public void setLesTraitements(List<Traitement> lesTraitements) {
        this.setLesTraitements(lesTraitements);
    }
    
    
    public String getIdProd() {
        return idProd;
    }

    public void setIdProd(String idProd) {
        this.idProd = idProd;
    }

    public String getNomprod() {
        return nomprod;
    }

    public void setNomprod(String nomprod) {
        this.nomprod = nomprod;
    }

    //</editor-fold>
}



