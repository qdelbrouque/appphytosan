
package entites;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Temporal;

@Entity

public class Pulverisation implements Serializable {
   
    @Id
    private Long  idPulv;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date  datePulv;
    private Float dosagePulv; // exprimé en kg par hectare

    public Pulverisation() { }

    public Pulverisation(Long idPulv, Date datePulv, Float dosagePulv) {
        
        this.idPulv = idPulv;
        this.datePulv = datePulv;
        this.dosagePulv = dosagePulv;
    }

    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public Long getIdPulv() {
        return idPulv;
    }
    
    public void setIdPulv(Long idPulv) {
        this.idPulv = idPulv;
    }
    
    public Date getDatePulv() {
        return datePulv;
    }
    
    public void setDatePulv(Date datePulv) {
        this.datePulv = datePulv;
    }
    
    public Float getDosagePulv() {
        return dosagePulv;
    }
    
    public void setDosagePulv(Float dosagePulv) {
        this.dosagePulv = dosagePulv;
    }
    //</editor-fold>
}

