package entites;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity

public abstract class Traitement implements Serializable {

    @Id
    private String   idTrait;
   
    @JoinColumn( name = "CODEPRODUIT")
    @ManyToOne
    private Produit  leProduit; 
    @JoinColumn( name = "CODEPARCELLE")
    @ManyToOne
    private Parcelle laParcelle;

    public Traitement() {}

    public Traitement(String idTrait, Produit leProduit, Parcelle laParcelle) {
        
        this.idTrait     = idTrait;
        this.leProduit   = leProduit;
        this.laParcelle  = laParcelle;
    }
    
    public abstract Float   quantiteAppliquee();
    
    
    public abstract void afficher();
    
    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public String getIdTrait() {
        return idTrait;
    }
    
    public void setIdTrait(String idTrait) {
        this.idTrait = idTrait;
    }
    
    public Produit getLeProduit() {
        return leProduit;
    }
    
    public void setLeProduit(Produit leProduit) {
        this.leProduit = leProduit;
    }
    
    public Parcelle getLaParcelle() {
        return laParcelle;
    }
    
    public void setLaParcelle(Parcelle laParcelle) {
        this.laParcelle = laParcelle;
    }
    //</editor-fold>
}


