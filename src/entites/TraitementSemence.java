package entites;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Temporal;

@Entity

public class TraitementSemence  extends Traitement{
   
    private Float dosageTraitementSemence; // exprimé en kg par hectare
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date  dateTraitementSemence;

    public TraitementSemence() {
    }

    // Constructeur
    public TraitementSemence( String idTrait, Produit leProduit, Parcelle laparcelle,
                              Float dosageTraitementSemence, Date dateTraitementSemence)
    {
       
        super(idTrait, leProduit, laparcelle);
        
        this.dosageTraitementSemence   = dosageTraitementSemence;
        this.dateTraitementSemence     = dateTraitementSemence;
    }
    
    // QuantiteAppliquee
    // On l'obtient en multipliant dosageTraitementSemence par la surface de la parcelle
    // concernée
    
    @Override
    public Float quantiteAppliquee() {
        
       return getLaParcelle().getSurface()*dosageTraitementSemence;
    }
        
     // Affichage du produit utilisé de la date d'application et de la quantité appliquée
    @Override
    public void afficher() {
        
        System.out.print("Traitement semence:");
        System.out.printf("Produit %-10s le %-10s %5.2f kg\n",
         
                getLeProduit().getNomprod(),
                utilitaires.UtilDate.dateVersChaine(getDateTraitementSemence()),
                quantiteAppliquee()
        );
    }
    
    //<editor-fold defaultstate="collapsed" desc="Gets & Sets">
    
    public Float getDosageTraitementSemence() {
        return dosageTraitementSemence;
    }
    
    public void setDosageTraitementSemence(Float dosageTraitementSemence) {
        this.dosageTraitementSemence = dosageTraitementSemence;
    }
    
    public Date getDateTraitementSemence() {
        return dateTraitementSemence;
    }
    
    public void setDateTraitementSemence(Date dateTraitementSemence) {
        this.dateTraitementSemence = dateTraitementSemence;
    }
    //</editor-fold>      
}
