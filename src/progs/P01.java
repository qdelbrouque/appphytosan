package progs;
import entites.Exploitation;
import java.util.Scanner;

public class P01 {

    public static void main(String[] args) {
        
        Scanner clavier= new Scanner(System.in);
        
        System.out.println("Identifiant de l'exploitant? ");
        String  identExploit = clavier.next();
       
        Exploitation exploitation= data.Dao.getLExploitation(identExploit);
       
        System.out.printf(
                          "\nExploitant: %-25s\n",
                          exploitation.getNomExploitant()
        );
        
        System.out.printf(
                          "Adresse de l'exploitation: %-20s\n",
                          exploitation.getAdrExploitant()
        );
        System.out.printf(
                          "Surface totale des parcelles cultivées: %5.2f ha\n",
                          exploitation.surfaceTotaleDesParcelles()
        );
        
        System.out.println();
        
        exploitation.afficherCultures();
        
        System.out.println();
    }
}






