
package progs;

import data.Dao;
import entites.Parcelle;
import entites.Pulverisation;
import entites.Traitement;
import entites.TraitementEnChamp;
import entites.TraitementSemence;
import java.util.Scanner;

public class P02 {

    public static void main(String[] args) {
        
        
        Scanner clavier= new Scanner(System.in);
        
        System.out.print("Identifiant de la parcelle? ");
        String  idParc = clavier.next();
        
        
        
        Parcelle parc= Dao.getLaParcelle(idParc);
        
        
        if(parc!=null) {
        
        
        System.out.printf(
                
                "\nExploitant: %-20s \nAdresse: %-20s\nEspèce cultivée: %-10s\nSurface: %5.2f ha\n\n",
                          
                parc.getlExploitation().getNomExploitant(),
                parc.getlExploitation().getAdrExploitant(),
                parc.getlEspeceCultivee().getNomEsp(),
                parc.getSurface()                   
       );
        
        
        for(Traitement  t : parc.getLesTraitements()){
        
            if (t instanceof TraitementSemence ){
            
                 TraitementSemence ts= (TraitementSemence ) t;   
                 System.out.print("Traitement semence:");
                 System.out.printf("Produit %-10s le %-10s %5.2f kg\n",
                      ts.getLeProduit().getNomprod(),
                      utilitaires.UtilDate.dateVersChaine(ts.getDateTraitementSemence()),
                      ts.quantiteAppliquee()
                 );
            }
            else{
           
                TraitementEnChamp tc= (TraitementEnChamp ) t;   
                
                System.out.print("Traitement en champ ");
                
                System.out.printf("Produit %-10s\n", tc.getLeProduit().getNomprod());
                 
                System.out.println("\n  Pulvérisations:\n");
                for (Pulverisation pulv : tc.getLesPulverisations()){
                 
                     System.out.printf("  le %-8s %6.2f kg\n",
                             
                       utilitaires.UtilDate.dateVersChaine(pulv.getDatePulv()),
                       pulv.getDosagePulv()*parc.getSurface()
                     ); 
                }
            
                
                System.out.printf("\n Quantité totale appliquée: %6.2f kg\n",
                       tc.quantiteAppliquee());
            }
            
            System.out.println();
        
        }
        
        }
        
        else System.out.println("\nPas de parcelle avec cet identifiant\n");
    }
}
