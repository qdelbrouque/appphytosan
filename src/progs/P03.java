
package progs;

import data.Dao;
import entites.Parcelle;
import entites.Traitement;
import java.util.Scanner;

public class P03 {

    public static void main(String[] args) {
       
        
        Scanner clavier= new Scanner(System.in);
        
        System.out.print("Identifiant de la parcelle? ");
        String  idParc = clavier.next();
        
        Parcelle parc= Dao.getLaParcelle(idParc);
        
        if(parc!=null) {
        
        System.out.printf(
                
                "\nExploitant: %-20s \nAdresse: %-20s\nEspèce cultivée: %-10s\nSurface: %5.2f ha\n\n",
                          
                parc.getlExploitation().getNomExploitant(),
                parc.getlExploitation().getAdrExploitant(),
                parc.getlEspeceCultivee().getNomEsp(),
                parc.getSurface()                   
        );
        
        
        for(Traitement  t : parc.getLesTraitements()){
        
            t.afficher(); System.out.println();
        
        }
        
        }
        
        else System.out.println("\nPas de parcelle avec cet identifiant\n");
    } 
        
    
}
